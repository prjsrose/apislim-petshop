# apislim-petshop

# Nesta versão apislimv01 foi desenvolvido:
* Entidade de usuários com auth;
* Autenticação salvando no banco com token(OAuth e JWT);
* PDO;
* Os métodos: GET, POST, PUT, DELETE, OPTIONS (Options método que mapeia a função de retorno de URI);
* Crud Funcional;
* Testes com Postmann estáo Ok e sendo tratado com status code - PetShop.postman_collection.json;
* Disponível no git https://bitbucket.org/prjsrose/apislimv01/src;
* Será disponibilizado no Heroku junto com o back-end