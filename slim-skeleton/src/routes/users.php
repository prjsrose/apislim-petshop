<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use App\Models\User;

//Get: http://localhost:8000/api/v1/users

$app->get('/api/v1/users' , function (Request $request, Response $response, array $args) {
    $users = User::get();
    return $response->withJson($users);
});