<?php
  //classes automaticamente;
  require __DIR__ . '/vendor/autoload.php';

  $settings = require __DIR__ . '/src/settings.php';
  $app = new \Slim\App($settings); //tudo que está configurado do slim aqui dentro;

  require __DIR__ . '/src/dependencies.php'; //trazer minha conexão

  $pdo = $container['db']->getPdo(); //chamo o pdo e armazeno minha conexão em uma var;

  //configurar o phinx;
  return [
    'paths' => [
        'migrations' => __DIR__ . '/src/db/migrations'
    ],
    'environments' => [
        'default_database' => 'development',
        'development' => [
            'name' => $container->get('settings')['db']['database'],
            'connection' => $pdo
        ]
    ]
];